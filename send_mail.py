# -*- coding: utf-8 -*- 
from __future__ import division, absolute_import, print_function, unicode_literals
import smtplib
import db
import json 
import logging
LOGGER = logging.getLogger(__name__)

from email.mime.text import MIMEText
from email.Header import Header
from email.Utils import formatdate

def create_massage(from_addr, to_addr, subject, body, encoding):
    msg = MIMEText(body.encode(encoding), 'plain', encoding)
    msg['Subject'] = Header(subject.encode(encoding), encoding)
    msg['From'] = from_addr
    msg['To'] = to_addr
    msg['Date'] = formatdate()
    return msg

def send_mail(from_addr, to_addr, subject, body, smtp_domain, login_name, password, encoding='iso-2022-jp'):
    msg = create_massage(from_addr, to_addr, subject, body, encoding)
    s = smtplib.SMTP(smtp_domain, 587)
    s.ehlo()
    s.login(login_name.encode("utf-8"), password.encode("utf-8"))
    try:
        s.sendmail(from_addr, to_addr, msg.as_string())
        LOGGER.info("メールを送信します")
    except:
        LOGGER.error("サーバが予期せぬエラーを返しました")
    finally:
        s.close()

from_address = "kanri_webmaster@tomonokai.net"
subject = "トモノカイ業務報告の移行について"
domain = "mail.tomonokai.net"
login = "kanri_system"
password = "lGsJb5ny1E"

for user_data in db.users:

    if user_data.job_finish_date is None:
        username = user_data.email.replace('@','__')
        to_address   = user_data.email
        text    = 'トモノカイの皆さん\nトモノカイ管理部です．\nこの度新しい業務報告システム報告アプリを作りましたので報告いたします．\n新しい業務報告のURL: http://www.tomo-report.com/accounts/login/ \n ユーザ名: ' + str(username) + '\n パスワード: tomonokai_pass\n ご登録お願いします\n \n管理部'
        send_mail(from_address, to_address, subject, text, domain, login, password)
