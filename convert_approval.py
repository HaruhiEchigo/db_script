#!/usr/bin/env python
# -*- coding: utf-8 -*-
import db
import json

from datetime import datetime
from utils import apply_tz, support_date


all_data_django = []
date_condition = datetime(2017, 4, 1)

for report_data in db.reports:
    if report_data.created_at > date_condition and report_data.state == 1:
        approval_dict = {}
        approval_dict["pk"] = str(report_data.id)
        approval_dict["model"] = "app.approval"
        fileds_dict = {}
        fileds_dict["manager"] = report_data.confirmed_by
        fileds_dict["report"] = report_data.id
        fileds_dict["judgment"] = True

        fileds_dict["created_at"] = apply_tz(report_data.confirmed_at)
        fileds_dict["updated_at"] = apply_tz(report_data.confirmed_at)

        approval_dict["fields"] = fileds_dict
        all_data_django.append(approval_dict)

# JSON データの書き込み
with open('json/approval.json', 'w') as f:
    data_json = json.dump(all_data_django, f,
                          default=support_date,
                          indent=2)
