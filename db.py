from sqlalchemy import create_engine, MetaData

engine = create_engine('sqlite:///rails.sqlite', echo=True)

metadata = MetaData(bind=engine, reflect=True)
table = metadata.tables['users']
users = table.select().execute().fetchall()

table = metadata.tables['worklogs']
reports = table.select().execute().fetchall()

table = metadata.tables['departments']
departments = table.select().execute().fetchall()

table = metadata.tables['projects']
projects = table.select().execute().fetchall()
