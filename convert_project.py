#!/usr/bin/env python
# -*- coding: utf-8 -*-
import db
import json

from utils import apply_tz, support_date


all_data_django = []

for project_data in db.projects:
    project_dict = {}
    project_dict["pk"] = str(project_data.id)
    project_dict["model"] = "app.project"
    fileds_dict = {}
    fileds_dict["title"] = project_data.name

    if project_data.end_date is None:
        fileds_dict["is_active"] = True
    else:
        fileds_dict["is_active"] = False

    fileds_dict["department"] = project_data.department_id
    fileds_dict["overview"] = u"概要:\n{}\n目的:\n{}".format(
                                                    project_data.description,
                                                    project_data.purpose)
    fileds_dict["start_date"] = project_data.start_date
    fileds_dict["created_at"] = apply_tz(project_data.created_at)
    fileds_dict["updated_at"] = apply_tz(project_data.updated_at)

    project_dict["fields"] = fileds_dict
    all_data_django.append(project_dict)

# JSON データの書き込み
with open('json/project.json', 'w') as f:
    data_json = json.dump(all_data_django, f,
                          default=support_date,
                          indent=2)
