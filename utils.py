# _*_coding:utf-8_*_

from pytz import timezone
from datetime import datetime, date


def apply_tz(date_time):
    """
    文字列のnaiveな日付けを受け取って、
    一度datetimeオブジェクトに変換した後
    timezoneをTokyoに設定して、再度文字列にして返す

    >> 何故このようなことをするのか？

    Djangoは1.4からawareなdatetimeオブジェクトのみを
    登録するように設計されているため、
    jsonでデータを取り込む際にはnaiveなデータをawareにする必要がある
    """

    jst = timezone("Japan")
    return jst.localize(date_time)


def support_date(obj):
    if isinstance(obj, datetime) or isinstance(obj, date):
        return obj.isoformat()
    else:
        raise TypeError
