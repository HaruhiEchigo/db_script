#!/usr/bin/env python
# -*- coding: utf-8 -*-
import db
import hashlib
from datetime import datetime
import json

# date型に対応するため
def support_datetime_default(o):
    if isinstance(o, datetime):
        return o.isoformat()

password_hash = hashlib.md5("password").hexdigest()
now_time = datetime.now().strftime("%Y/%m/%d %H:%M:%S")
users = db.users
all_data_django = []
for user_data in db.users:
    # 退職者はデータ移行しない
    if user_data.job_finish_date is None:
        user_dict = {}
        user_dict["pk"] = str(user_data.id)
        user_dict["model"] = "auth.user"
        fileds_dict = {}
        # メールアドレスからユーザネームを生成
        username = user_data.email.replace('@', '__') 
        fileds_dict["username"] = username
        fileds_dict.update({"first_name": "", "last_name": "", "is_active": False, "is_superuser": False, "is_staff": True})
        fileds_dict["last_login"] = user_data.last_sign_in_at  
        fileds_dict["groups"] = []  
        fileds_dict["password"] = password_hash
        fileds_dict["date_joined"] = now_time
        fileds_dict["email"] = user_data.email
        fileds_dict["user_permissions"] = []
        user_dict["fields"] = fileds_dict
        all_data_django.append(user_dict)

        # ユーザプロファイルの生成
        profile_dict = {}
        profile_dict["pk"] = str(user_data.id)
        profile_dict["model"] = "app.userProfile"
        fileds_dict = {}
        fileds_dict["user"] = user_data.id
        fileds_dict["name"] = user_data.name
        fileds_dict["manager_flag"] = user_data.last_sign_in_at  
        fileds_dict["department"] = user_data.department_id  
        fileds_dict["tell"] = user_data.phone_number 
        fileds_dict["address"] = user_data.address
        fileds_dict["tnews_id"] = user_data.tnewsid
        fileds_dict["university"] = user_data.university
        fileds_dict["faculty"] = user_data.major  
        fileds_dict["birth_date"] = user_data.birthday  
        fileds_dict["entry_date"] = user_data.job_start_date
        fileds_dict["current_wage"] = user_data.payment_by_hour
        fileds_dict["previouse_wage"] = None
        fileds_dict["next_wage"] = None  
        fileds_dict["departure_station"] = user_data.src_station  
        fileds_dict["arrival_station"] = user_data.dst_station
        fileds_dict["carfare"] = user_data.transportation_fee
        fileds_dict["staff_type"] = user_data.staff_type
        fileds_dict["tax_withholding"] = user_data.withholding_type
        fileds_dict["num_of_consort"] = user_data.mate_num  
        fileds_dict["num_of_supported"] = user_data.dependent_num
        fileds_dict["model_confirmed"] = False
         
        profile_dict["fields"] = fileds_dict
        all_data_django.append(profile_dict)

# JSON データの書き込み
data_json = json.dumps(all_data_django, ensure_ascii=False, default=support_datetime_default, indent=2)
f2 = open('user.json', 'w')
json.dump(data_json, f2)
print(data_json)
