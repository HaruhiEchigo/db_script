#!/usr/bin/env python
# -*- coding: utf-8 -*-
import db
import json

from datetime import datetime
from utils import apply_tz, support_date


all_data_django = []
date_condition = datetime(2017, 4, 1)

for report_data in db.reports:
    if report_data.created_at > date_condition:
        report_dict = {}
        report_dict["pk"] = str(report_data.id)
        report_dict["model"] = "app.report"
        fileds_dict = {}
        fileds_dict["project"] = report_data.project_id
        fileds_dict["staff"] = report_data.user_id

        if report_data.fixed_salary_flag == 0:
            work_hours = float(report_data.work_minutes) / 60;
            fileds_dict["working_hours"] = work_hours
            if work_hours > 0:
                fileds_dict["hourly_wage"] = int(float(report_data.salary) / work_hours)
            else:
                # 何故か0時間で申請されている業務報告があるので、
                # これは時給0で設定
                fileds_dict["hourly_wage"] = 0
        else:
            fileds_dict["working_hours"] = None
            fileds_dict["hourly_wage"] = report_data.salary

        fileds_dict["departure_station"] = report_data.source
        fileds_dict["arrival_station"] = report_data.destination
        fileds_dict["carfare"] = report_data.fare
        fileds_dict["worked_at"] = report_data.work_date
        fileds_dict["created_at"] = apply_tz(report_data.created_at)
        fileds_dict["updated_at"] = apply_tz(report_data.updated_at)
        fileds_dict["detail"] = report_data.content

        report_dict["fields"] = fileds_dict
        all_data_django.append(report_dict)

# JSON データの書き込み
with open('json/report.json', 'w') as f:
    data_json = json.dump(all_data_django, f,
                          default=support_date,
                          indent=2)
