#!/usr/bin/env python
# -*- coding: utf-8 -*-
import db
import json

from utils import support_date

all_data_django = []

for department_data in db.departments:
    department_dict = {}
    department_dict["pk"] = str(department_data.id)
    department_dict["model"] = "app.department"
    fileds_dict = {}
    fileds_dict["name"] = department_data.name
    fileds_dict["is_active"] = True

    department_dict["fields"] = fileds_dict
    all_data_django.append(department_dict)

# JSON データの書き込み
with open('json/department.json', 'w') as f:
    data_json = json.dump(all_data_django, f,
                          default=support_date,
                          indent=2)
