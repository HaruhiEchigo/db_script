#!/usr/bin/env python
# -*- coding: utf-8 -*-
import db
import json

from datetime import datetime
from utils import apply_tz, support_date

# 初回パスワードは決め打ち
password_hash = "pbkdf2_sha256$30000$7VVd0INCFMLU$LFYETcMR+eHSC39tCmOaHI+WGzA9XSSz7zUXpO+TJ4A="

now_time = datetime.now()
all_data_django = []

for user_data in db.users:
    # 退職者はデータ移行しない
    if user_data.job_finish_date is None:
        user_dict = {}
        user_dict["pk"] = str(user_data.id)
        user_dict["model"] = "auth.user"
        fileds_dict = {}
        # メールアドレスからユーザネームを生成
        username = user_data.email.replace('@', '__')
        fileds_dict["username"] = username
        fileds_dict.update({"first_name": "", "last_name": "", "is_active": False, "is_superuser": False, "is_staff": True})
        fileds_dict["groups"] = []
        fileds_dict["is_active"] = True
        fileds_dict["password"] = password_hash
        fileds_dict["date_joined"] = apply_tz(now_time)
        fileds_dict["email"] = user_data.email
        fileds_dict["user_permissions"] = []
        user_dict["fields"] = fileds_dict
        all_data_django.append(user_dict)

        # ユーザプロファイルの生成
        profile_dict = {}
        profile_dict["pk"] = str(user_data.id)
        profile_dict["model"] = "app.userProfile"
        fileds_dict = {}
        fileds_dict["user"] = user_data.id
        fileds_dict["name"] = user_data.name

        if user_data.staff_type == 3:
            fileds_dict["manager_flag"] = True
            fileds_dict["staff_type"] = 0
        else:
            fileds_dict["manager_flag"] = False
            fileds_dict["staff_type"] = user_data.staff_type

        fileds_dict["department"] = user_data.department_id
        fileds_dict["tell"] = user_data.phone_number
        fileds_dict["address"] = user_data.address

        if user_data.tnewsid:
            fileds_dict["tnews_id"] = user_data.tnewsid
        else:
            fileds_dict["tnews_id"] = None

        fileds_dict["university"] = user_data.university
        fileds_dict["faculty"] = user_data.major
        fileds_dict["birth_date"] = user_data.birthday

        if user_data.job_start_date:
            fileds_dict["entry_date"] = user_data.job_start_date.date()

        fileds_dict["current_wage"] = user_data.payment_by_hour
        fileds_dict["departure_station"] = user_data.src_station
        fileds_dict["arrival_station"] = user_data.dst_station
        fileds_dict["carfare"] = user_data.transportation_fee
        fileds_dict["tax_withholdings"] = user_data.withholding_type

        if user_data.mate_num:
            fileds_dict["num_of_consort"] = user_data.mate_num
        else:
            fileds_dict["num_of_consort"] = 0

        if user_data.dependent_num:
            fileds_dict["num_of_supported"] = user_data.dependent_num
        else:
            fileds_dict["num_of_supported"] = 0

        fileds_dict["modal_confirmed"] = False

        profile_dict["fields"] = fileds_dict
        all_data_django.append(profile_dict)


# JSON データの書き込み
with open('json/user.json', 'w') as f:
    data_json = json.dump(all_data_django, f,
                          default=support_date,
                          indent=2)
