# -*- coding: utf-8 -*- 

import smtplib
import db
from email.mime.text import MIMEText
from email.Header import Header
from email.Utils import formatdate

from_address = 'kanri_webmaster@tomonokai.net'
subject = u'トモノカイ業務報告の移行について'
domain = "mail.tomonokai.net"

for user_data in db.users:
    username = user_data.email.replace('@','__')
    to_address   = user_data.email

    charset = 'ISO-2022-JP'
    text    = 'トモノカイの皆さん\nトモノカイ管理部です．\nこの度新しい業務報告システム報告アプリを作りましたので報告いたします．\n新しい業務報告のURL: http://www.tomo-report.com/accounts/login/ \n ユーザ名: ' + str(username) + '\n パスワード: tomonokai_pass\n ご登録お願いします\n \n管理部'
    print(text)
    msg = MIMEText(text.encode(charset), 'plain', charset)
    msg['Subject'] = Header(subject, charset)
    msg['From'] = from_address
    msg['To'] = to_address
    msg['Date'] = formatdate(localtime=True)

    smtp = smtplib.SMTP('mail.tomonokai.net')
    smtp.sendmail(from_address, to_address, msg.as_string())
    smtp.close()
    break
