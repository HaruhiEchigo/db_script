# _*_coding:utf-8_*_

from fabric.api import local, task, env

import glob
from fabric.contrib.project import rsync_project

REMOTE_FIXTURE_PATH = "/var/www/tomo-report.com/source/tomonokai_attendance_management/fixtures"

env.use_ssh_config = True
env.hosts = ["tomonokai"]

@task
def convert():
    for path in glob.glob("./convert_*.py"):
        local("python {}".format(path))


@task
def send_json():
    rsync_project(
            local_dir="./json/",
            remote_dir=REMOTE_FIXTURE_PATH,
    )
